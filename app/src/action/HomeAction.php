<?php
namespace App\Action;

use Slim\Views\Twig;
use Psr\Log\LoggerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Illuminate\Database\Capsule\Manager as DB;
use Faker\Factory as Faker;
use League\Plates;

final class HomeAction
{
    private $view;
    private $logger;
    private $pc;
    public function __construct(Twig $view, LoggerInterface $logger, $pc)
    {
        $this->view = $view;
        $this->logger = $logger;
        $this->console = $pc;
        $this->tmpl = new \PlatesModel;
        $this->pc = \PhpConsole\Handler::getInstance();
    }

    public function __invoke(Request $request, Response $response, $args)
    {
        // $this->logger->info("Home page action dispatched");
        //$this->readM3u8("https://cdn.imusm-studio.com/hls/test.m3u8");
        $data['hello'] = 'hello world';
        $this->view->render($response, 'home.twig', $data);
	    //$str = $this->view->render('home.twig', $data);
	    //echo $str;
        //$logger = new \Monolog\Logger('all', array(new \Monolog\Handler\PHPConsoleHandler()));
        //\Monolog\ErrorHandler::register($logger);
        //echo $undefinedVar;
        //$logger->debug('SELECT * FROM users', array('db', 'time' => 0.012));
        //$logger->debug('server', $_SERVER); // PHP Console debugger for any type of vars
        //$this->fp->debug('中文', array("中文","ABC", "123", "中文二"));
        \PC::debug(array('中文', 'abc', '123'), 'tag');
        \PC::tag($var);
        $this->console->debug(array('中文', 'abc', '123'), 'tag');
        //return $response;
                
    }
    public function users(Request $request, Response $response, $args)
    {
        //$chkPlayed = DB::table("users")->count();
        //echo $chkPlayed;
        
        //$courses = DB::table('users')->skip(1)->take(10)->get();
        //dd(DB::getQueryLog());
        //$this->fp->info('SQL', DB::getQueryLog());
        //$data = $courses->toJson();
        //$response->withHeader('Content-type', 'application/json');
        //return $response->withJson($courses, 200,  JSON_NUMERIC_CHECK);
        // $faker = new Faker;
        // $faker_result = $faker->create();
        // echo $faker_result->name;
        // echo $faker_result->address;
        // echo urldecode("%26");
    }
    public function tmpl2str($tmpl, $data)
    {
        require_once '/path/to/vendor/twig/twig/lib/Twig/Autoloader.php';
        \Twig_Autoloader::register();
        $loader = new \Twig_Loader_Filesystem('/path/to/app/templates');
        $twig = new \Twig_Environment($loader, array(
            'cache' => '/path/to/cache',
            'auto_reload' => true
        ));
        $twig = new \Twig_Environment($loader);
        $str = $twig->render($tmpl, $data);
        return $str;
    }
    
}
