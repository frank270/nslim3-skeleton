<?php
class PlatesModel extends \League\Plates\Engine
{
    /**
     * s
     * @var string
     */
    protected $tmplPath = '/path/to/app/templates';
    public function loadView($tmpl, $data)
    {
        $templates = new PlatesModel($this->tmplPath);
        return  $templates->render($tmpl, $data);
    }
}
