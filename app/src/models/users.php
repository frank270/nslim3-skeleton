<?php
use Illuminate\Database\Capsule\Manager as DB;
class Users extends \Illuminate\Database\Eloquent\Model{
    /**
     * s
     * @var string
     */
    protected $table = 'users';
    /**
     * [$primaryKey description]
     * @var string
     */
    protected $primaryKey  = 'no';

    /**
     * [$timestamps description]
     * @var boolean
     */
    public $timestamps = true;
    protected $allowedFields = array(
        'col1',     
        'col2' 
    );
}