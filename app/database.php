<?php
/**
 * Database setting
 * 
 * Long description for file (if any)...
 *
 * PHP version 5
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to license@php.net so we can mail you a copy immediately.
 *
 * @category   CategoryName
 * @package    PackageName
 * @author     Original Author <author@example.com>
 * @author     Another Author <another@example.com>
 * @copyright  1997-2005 The PHP Group
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    GIT:
 * @link       http://pear.php.net/package/PackageName
 * @see        NetOther, Net_Sample::Net_Sample()
 * @since      File available since Release 1.2.0
 * @deprecated File deprecated in Release 2.0.0
 */
use Illuminate\Database\Capsule\Manager as Capsule;
$dbcfg['prod']= array(
        'driver'    => 'mysql',
        'host'      => '172.31.20.253',
        'database'  => 'db',
        'username'  => 'dbuser',
        'password'  => 'dbpwd',
        'charset'   => 'utf8',
        'collation' => 'utf8_unicode_ci',
        'prefix'    => ''
    );
$dbcfg['test']= array(
        'driver'    => 'mysql',
        'host'      => 'localhost',
        'database'  => 'db',
        'username'  => 'dbuser',
        'password'  => 'dbpwd',
        'charset'   => 'utf8',
        'collation' => 'utf8_unicode_ci',
        'prefix'    => ''
    );
$dbcfg['stage']= array(
        'driver'    => 'mysql',
        'host'      => 'localhost',
        'database'  => 'db',
        'username'  => 'dbuser',
        'password'  => 'dbpwd',
        'charset'   => 'utf8',
        'collation' => 'utf8_unicode_ci',
        'prefix'    => ''
    );
$capsule = new Capsule; 

$capsule->addConnection($dbcfg[ENV]);
/*
// Set the event dispatcher used by Eloquent models... (optional)
use Illuminate\Events\Dispatcher;
use Illuminate\Container\Container;
$capsule->setEventDispatcher(new Dispatcher(new Container));
*/
// Make this Capsule instance available globally via static methods... (optional)
$capsule->setAsGlobal();
// Setup the Eloquent ORM... (optional; unless you've used setEventDispatcher())
$capsule->bootEloquent();
//enable Query log for debug!
$capsule::connection()->enableQueryLog();
