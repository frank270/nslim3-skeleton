<?php
// DIC configuration

$container = $app->getContainer();

// -----------------------------------------------------------------------------
// Service providers
// -----------------------------------------------------------------------------

// Twig
$container['view'] = function ($c) {
    $settings = $c->get('settings');
    $view = new Slim\Views\Twig($settings['view']['template_path'], $settings['view']['twig']);

    // Add extensions
    $view->addExtension(new Slim\Views\TwigExtension($c->get('router'), $c->get('request')->getUri()));
    $view->addExtension(new Twig_Extension_Debug());

    return $view;
};

// Flash messages
$container['flash'] = function ($c) {
    return new Slim\Flash\Messages;
};

// -----------------------------------------------------------------------------
// Service factories
// -----------------------------------------------------------------------------

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings');
    $logger = new Monolog\Logger($settings['logger']['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['logger']['path'], Monolog\Logger::DEBUG));
    return $logger;
};
/*firephp 
$container['fp'] = function ($c) {
    $fp = new Monolog\Logger('firephp');
    $fp->pushHandler(new Monolog\Handler\FirePHPHandler(Monolog\Logger::DEBUG));
    return $fp;
};
*/
$container['pc'] = function ($c) {
    $handler = PhpConsole\Handler::getInstance();
    PhpConsole\Helper::register(); // it will register global PC class
    $handler->start(); // start handling PHP errors & exceptions
    $handler->getConnector()->setSourcesBasePath($_SERVER['DOCUMENT_ROOT']); 
    return $handler;
};
// -----------------------------------------------------------------------------
// Action factories
// -----------------------------------------------------------------------------

$container[App\Action\HomeAction::class] = function ($c) {
    return new App\Action\HomeAction($c->get('view'), $c->get('logger'), $c->get('pc'));
};
$container[App\Action\Member::class] = function ($c) {
    return new App\Action\Member($c->get('view'), $c->get('logger'));
};
