<?php

// To help the built-in PHP dev server, check if the request was actually for
// something which should probably be served as a static file
if (PHP_SAPI === 'cli-server' && $_SERVER['SCRIPT_FILENAME'] !== __FILE__) {
    return false;
}
$server_ip = $_SERVER['SERVER_ADDR'];
switch($server_ip){
case '127.0.0.1':
    define('ENV', 'test');
    break;
case '172.30.0.76':
    define('ENV', "prod");
    break;
default:
    define('ENV', "stage");
    break;
}
require __DIR__ . '/../vendor/autoload.php';

session_start();

// Instantiate the app
$settings = require __DIR__ . '/../app/settings.php';
$app = new \Slim\App($settings);

// Set up dependencies
require __DIR__ . '/../app/dependencies.php';

// Register middleware
require __DIR__ . '/../app/middleware.php';

// Register database.php
require __DIR__ . '/../app/database.php';

//register Illuminate database model or others
foreach (glob(__DIR__ . '/../app/src/models/*.php') as $modelFile) {
    require $modelFile;
}

//others controll
foreach (glob(__DIR__ . '/../app/src/action/*.php') as $actionFile) {
    require $actionFile;
}

// Register routes
require __DIR__ . '/../app/routes.php';

// Run!
$app->run();
